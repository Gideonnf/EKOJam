﻿// Requires DoTween to work

//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.EventSystems;
//using UnityEngine.UI;

//[RequireComponent(typeof(ScrollRect))]
//public class SnapScrollRect : MonoBehaviour, IBeginDragHandler, IEndDragHandler
//{

//    public event Action<int> OnItemCentered = delegate { };

//    public float time = 1f;
//    public Ease ease = Ease.OutExpo;

//    private ScrollRect scrollRect;
//    private Tweener tweener;

//    void Start()
//    {
//        scrollRect = GetComponent<ScrollRect>();
//        scrollRect.inertia = false;
//    }

//    public void OnBeginDrag(PointerEventData eventData)
//    {
//        if (tweener != null && tweener.IsActive())
//        {
//            tweener.Kill();
//        }
//    }

//    public void OnEndDrag(PointerEventData eventData)
//    {
//        int itemCount = scrollRect.content.childCount;
//        float step = 1;

//        if (itemCount > 2)
//        {
//            step = 1f / (itemCount - 1);
//        }

//        float target = Mathf.Clamp01(Mathf.RoundToInt(ScrollValue / step) * step);

//        tweener = DOTween.To(() => ScrollValue, (v) => ScrollValue = v, target, time).SetEase(ease);

//        int index = Mathf.RoundToInt(target * (itemCount - 1));
//        OnItemCentered(index);
//    }

//    private float ScrollValue
//    {
//        get
//        {
//            return scrollRect.horizontal ?
//                scrollRect.horizontalNormalizedPosition :
//                scrollRect.verticalNormalizedPosition;
//        } 

//        set
//        {
//            if (scrollRect.horizontal)
//            {
//                scrollRect.horizontalNormalizedPosition = value;
//            }
//            else
//            {
//                scrollRect.verticalNormalizedPosition = value;
//            }
//        }
//    }
//}
