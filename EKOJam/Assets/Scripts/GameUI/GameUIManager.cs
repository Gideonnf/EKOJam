﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUIManager : MonoBehaviour
{
    public static GameUIManager Instance = null;

    [Header("Carbon")]
    [SerializeField]
    GameObject carbonEmissionBar = null;
    [SerializeField]
    float maxCarbonYScale = 0.0f;
    [Header("Energy")]
    [SerializeField]
    GameObject energyBar = null;
    [SerializeField]
    float maxEnergyYScale = 0.0f;
    // Used to check if we still need to Update
    bool stillUpdate = true;


    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);

        SceneManager.sceneLoaded += GoToLoseScene;
    }
    void GoToLoseScene(Scene scene, LoadSceneMode mode)
    {
        if (scene.name != "LoseScene")
            return;

        GameObject text = GameObject.FindGameObjectWithTag("Player");
        if (carbonEmissionBar.GetComponent<RectTransform>().localScale.y >= maxCarbonYScale)
            text.GetComponent<TextMeshProUGUI>().text = "You Produced too much Carbon :(";
        else
            text.GetComponent<TextMeshProUGUI>().text = "You Fainted due to starvation :(";

        stillUpdate = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!stillUpdate)
            return;

        // Updates the Carbon Emission UI
        float current = PlayerManager.instance.GetComponent<PlayerStats>().GetCarbonEmission();
        float max = PlayerManager.instance.GetComponent<PlayerStats>().GetMaxCarbonEmission();

        Vector3 newScale = carbonEmissionBar.GetComponent<RectTransform>().localScale;
        newScale.y = (current / max) * maxCarbonYScale;
        carbonEmissionBar.GetComponent<RectTransform>().localScale = newScale;

        // check when should we end the game
        if (current >= max)
            SceneManager.LoadScene("LoseScene");


        // Updates the Energy UI
        current = PlayerManager.instance.GetComponent<PlayerStats>().GetHunger();
        max = PlayerManager.instance.GetComponent<PlayerStats>().GetMaxHunger();

        newScale = energyBar.GetComponent<RectTransform>().localScale;
        newScale.y = (current / max) * maxCarbonYScale;
        energyBar.GetComponent<RectTransform>().localScale = newScale;


        // check when should we end the game
        if (current <= 0.0f)
            SceneManager.LoadScene("LoseScene");
    }
}
