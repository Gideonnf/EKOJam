﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeScript : MonoBehaviour
{
    public static CameraShakeScript instance = null;


    public void Awake()
    {
        instance = this;
    }

    public void Update()
    {

    }

    public IEnumerator Shake (float duration, float magnitude)
    {
        Vector3 originalPos = transform.localPosition;
        float elapsed = 0.0f;

        while(elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.localPosition += new Vector3(x, y, 0.0f);

            elapsed += Time.deltaTime;

            // Return back to the original Position
            //transform.localPosition = originalPos;
            // Break out of the coroutine here
            yield return null;
        }

        // Return back to the original Position
        transform.localPosition = originalPos;
    }
}
