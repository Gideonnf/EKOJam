﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    // The public Instance
    public static ObstacleManager Instance = null;

    // Ground level
    [SerializeField]
    float groundLevel = 0.0f;
    // Used to store all the Obstacles to spawn
    [SerializeField]
    List<ObstacleBaseClass> listOfObstacles = new List<ObstacleBaseClass>();
    // Odds of spawing Obstacle
    public float spawningChance = 0.4f;
    float internalTime = 10.0f;
    float internalTimer = 0.0f;
    // Odds of spawning something else afterwards
    public float afterspawnChance = 0.5f;


    // Used to store all the Obstacles that need to 
    // to have an after spawn effect
    List<ObstacleBaseClass> listOfAfterSpawning = new List<ObstacleBaseClass>();


    // Attach Instance
    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;

        // Get total chance
        float totalPer = 0.0f;
        foreach (ObstacleBaseClass obst in listOfObstacles)
        {
            obst.obstSpawnChance = obst.GetEditorSpawnChance();
            totalPer += obst.obstSpawnChance;
        }
        // Get the actual percentages
        float previousPer = 0.0f;
        foreach (ObstacleBaseClass obst in listOfObstacles)
        {
            if (obst.obstSpawnChance == 0.0f)
                continue;

            obst.minSpawnRange = previousPer;
            obst.obstSpawnChance = previousPer + (obst.obstSpawnChance / totalPer);

            previousPer = obst.obstSpawnChance;
        }
    }

    private void Update()
    {
        internalTimer -= Time.deltaTime;
        if(internalTimer < 0.0f)
        {
            spawningChance += 0.1f;
            spawningChance = Mathf.Clamp(spawningChance, 0.4f, 0.7f);

            internalTimer = internalTime;
        }
    }


    // Attempts to spawn a random Obstacle if chance allows it
    public void SpawnRandomObstacle(GameObject parentObject, Vector3 centerLanePos)
    {
        bool stopLoop = false;
        bool spawnAfter = false;
        int loopCount = 0;
        float percentage = 0.0f;

        // Cache the current spawn percentage
        percentage = Random.value;
        // Do we even spawn
        if (percentage > spawningChance)
            return;


        // Set the ground position
        Vector3 centerPosition = centerLanePos;
        centerPosition.y = groundLevel;
        GameObject newestObstacle = null;
        

        // Check which object to spawn
        while (!stopLoop)
        {
            // Cache the current percentage
            percentage = Random.value;
            // Loop through all the obstacle's percentages
            foreach (ObstacleBaseClass obst in listOfObstacles)
            {
                // if no spawn chance, skip
                if (obst.obstSpawnChance == 0.0f)
                    continue;
                // Once reached a percentage, we spawn
                if (obst.minSpawnRange <= percentage && percentage < obst.obstSpawnChance)
                {
                    PathManager.PathReturnData returnData = PathManager.Instance.GetAvaliableRandomPath(centerPosition);
                    // If no path, then just return
                    if(returnData.pathDirEnum == PathManager.PATHS.P_TOTAL)
                    {
                        // break out of the loop
                        stopLoop = true;
                        break;
                    }

                    // Do we want to spawn something else afterwards
                    if (Random.value <= afterspawnChance)
                        spawnAfter = true;
                    else
                        spawnAfter = false;
                    // TEST SPAWNNN
                    newestObstacle = SpawnObstacle(obst.GetObstacleID(), returnData.position, returnData.pathDirEnum, spawnAfter);
                    newestObstacle.transform.parent = parentObject.transform;

                    // break out of the loop
                    stopLoop = true;
                    break;
                }
                    
            }

            // Fail safe in case we are extremely Unlucky
            loopCount++;
            if (loopCount >= 20)
            {
                PathManager.PathReturnData returnData = PathManager.Instance.GetAvaliableRandomPath(centerPosition);
                // If no path, then just return
                if (returnData.pathDirEnum == PathManager.PATHS.P_TOTAL)
                {
                    // break out of the loop
                    stopLoop = true;
                    break;
                }

                // TEST SPAWNNN
                newestObstacle = SpawnObstacle(0, returnData.position, returnData.pathDirEnum, false);
                newestObstacle.transform.parent = parentObject.transform;
                // break out of loop
                break;
            }
        }
    }

    // Spawns a Obstacle based on what ID you pass in 
    // AND Adds it into the AfterEffects list for more Obstacles, if you want
    public GameObject SpawnObstacle(ObstacleBaseClass.SPAWN_AFTER obstacleID, Vector3 spawnPos, PathManager.PATHS dir, bool addToAfterSpawn = true)
    {
        // Empty
        GameObject spawnedObstacle = null;
        // Fetch GO
        switch (obstacleID)
        {
            case ObstacleBaseClass.SPAWN_AFTER.SA_JUMP_OVER:
                spawnedObstacle = ObjectPooler.instance.FetchGO("ObstacleJumpOver");
                break;
            case ObstacleBaseClass.SPAWN_AFTER.SA_SLIDE_UNDER:
                spawnedObstacle = ObjectPooler.instance.FetchGO("ObstacleSlideUnder");
                break;
            case ObstacleBaseClass.SPAWN_AFTER.SA_UNPASSABLE:
                spawnedObstacle = ObjectPooler.instance.FetchGO("ObstacleWall");
                break;
        }
        // set position
        spawnedObstacle.transform.position = spawnPos;
        // Set where we spawned and which direction
        spawnedObstacle.GetComponent<ObstacleBaseClass>().spawnedPos = spawnPos;
        spawnedObstacle.GetComponent<ObstacleBaseClass>().myPathDirEnum = dir;
        //if (addToAfterSpawn)
        //    spawnedObstacle.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = testCube;
        //else
        //    spawnedObstacle.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = testCyyy;


        // If we don't want to add to list, just create and return;
        if (!addToAfterSpawn)
            return spawnedObstacle;

        // Randomise which Obstacle to spawn after
        ObstacleBaseClass.SPAWN_AFTER spawnAfterChoice = (ObstacleBaseClass.SPAWN_AFTER)Random.Range((int)ObstacleBaseClass.SPAWN_AFTER.SA_JUMP_OVER, (int)ObstacleBaseClass.SPAWN_AFTER.SA_TOTAL);


        // Which obstacle are we spawning
        switch (obstacleID)
        {
            // Jump Over
            case ObstacleBaseClass.SPAWN_AFTER.SA_JUMP_OVER:
                {
                    // set the after spawn variation
                    spawnedObstacle.GetComponent<ObstacleBaseClass>().SetAfterSpawnSettings(ObstacleBaseClass.SPAWN_AFTER.SA_JUMP_OVER, dir);
                    // Add the spawned Obstacle to the list
                    listOfAfterSpawning.Add(spawnedObstacle.GetComponent<ObstacleBaseClass>());
                }
                break;
            // Slide Under
            case ObstacleBaseClass.SPAWN_AFTER.SA_SLIDE_UNDER:
                {
                    // set the after spawn variation
                    spawnedObstacle.GetComponent<ObstacleBaseClass>().SetAfterSpawnSettings(ObstacleBaseClass.SPAWN_AFTER.SA_SLIDE_UNDER, dir, 3);
                    // Add the spawned Obstacle to the list
                    listOfAfterSpawning.Add(spawnedObstacle.GetComponent<ObstacleBaseClass>());
                }
                break;
        }

        return spawnedObstacle;
    }


    // Updates the After Spawn loop
    public void UpdateAfterSpawn()
    {
        // Remove those that are not doing afterspawn
        for(int i = listOfAfterSpawning.Count-1; i >= 0; --i)
        {
            if (!listOfAfterSpawning[i].GetAfterSpawning())
                listOfAfterSpawning.RemoveAt(i);
        }

        foreach (ObstacleBaseClass item in listOfAfterSpawning)
        {
            item.UpdateVariationType();
        }
    }


    // Returns the Ground Level
    public float GetGroundLevel()
    {
        return groundLevel;
    }
}
