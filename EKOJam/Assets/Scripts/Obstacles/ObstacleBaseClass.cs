﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBaseClass : MonoBehaviour
{
    // What to spawn after we have spawned this Obstacle
    public enum SPAWN_AFTER
    {
        SA_JUMP_OVER,
        SA_SLIDE_UNDER,
        SA_UNPASSABLE,

        SA_TOTAL
    }
    GameObject cameraObject = null;
    [Header("Base Class")]
    [System.NonSerialized]
    public float minSpawnRange = 0.0f;
    [SerializeField]
    float editorSpawnChance = 0.333f;    // Used for editor to modify
    [System.NonSerialized]  // Used to store the actual percentages
    public float obstSpawnChance = 0.333f;
    [SerializeField]
    // Used to Identify what Obstacle is this
    protected SPAWN_AFTER obstacleID = SPAWN_AFTER.SA_TOTAL;
    // Other Spawning Data
    [System.NonSerialized]
    public Vector3 spawnedPos;
    [System.NonSerialized]
    public PathManager.PATHS myPathDirEnum;
    // What to do after spawning
    SPAWN_AFTER whatToSpawn;
    int turnsToWait;
    PathManager.PATHS spawnPathEnum;
    bool afterSpawningUsed = false; // are we even doing anything after spawning object


    private void Start()
    {
        cameraObject = Camera.main.gameObject;
    }
    private void Update()
    {
        if (transform.position.z - cameraObject.transform.position.z < 0.1f)
        {
            transform.parent = null;
            transform.gameObject.SetActive(false);
            //Debug.LogWarning("DELETED!");
        }
    }


    public void SetAfterSpawnSettings(SPAWN_AFTER spawn, PathManager.PATHS whatPath, int wait = 2)
    {
        afterSpawningUsed = true;

        whatToSpawn = spawn;
        // Randomise path or set path
        if (whatPath != PathManager.PATHS.P_TOTAL)
            spawnPathEnum = whatPath;
        else
        {
            spawnPathEnum = (PathManager.PATHS)Random.Range((int)PathManager.PATHS.P_LEFT, (int)PathManager.PATHS.P_RIGHT);
        }
        turnsToWait = wait;
    }
    public bool UpdateVariationType()
    {
        if(!afterSpawningUsed)
            return false;
        // Decrement turns left to wait
        turnsToWait--;

        // check if it's time to spawn the object
        if(turnsToWait < 0)
        {
            // Change position to be above the ground
            Vector3 newPos = MapGenerator.Instance.GetFirstMostTilePosition();
            newPos.y = ObstacleManager.Instance.GetGroundLevel();
            // Check if we need to change direction
            if (spawnPathEnum == PathManager.PATHS.P_LEFT)
                newPos.x -= PathManager.Instance.GetWidthBetweenPaths();
            else if(spawnPathEnum == PathManager.PATHS.P_RIGHT)
                newPos.x += PathManager.Instance.GetWidthBetweenPaths();
            // Spawn the Object and parent under the newest tile
            GameObject newestObstacle = ObstacleManager.Instance.SpawnObstacle(whatToSpawn, newPos, spawnPathEnum, false);
            newestObstacle.transform.parent = MapGenerator.Instance.GetFirstMostTile();

            afterSpawningUsed = false;
            return true;
        }

        return false;
    }


    // Return the Obstacle ID
    public SPAWN_AFTER GetObstacleID()
    {
        return obstacleID;
    }
    // Return whether we are doing after spawning
    public bool GetAfterSpawning()
    {
        return afterSpawningUsed;
    }
    // Get Editor Spawn Chance
    public float GetEditorSpawnChance()
    {
        return editorSpawnChance;
    }

    // Do Collision here, since all does the same thing..
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //Debug.LogWarning("AHHHHHHHH");

            // Deduct hunger
            PlayerManager.instance.GetComponent<PlayerStats>().ModifyHunger(-5);
            // Camera Shake
            StartCoroutine(CameraShakeScript.instance.Shake(0.3f, 0.01f));
        }
    }
}
