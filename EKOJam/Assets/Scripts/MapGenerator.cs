﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public static MapGenerator Instance = null;

    // Map Generator Settings
    [Header("Generator Settings")]
    [SerializeField]
    int minimumVisbileTiles = 50;
    [SerializeField]
    int minimumStartObstacle = 20;  // Wheb generating Starting tiles, at which point do we start generating the obstacles too
    [Header("When to Reset Map Position")]
    [SerializeField]
    float MaxResetRange = 1000.0f;

    // The Map Tiles to instantiate
    [Header("Default Map Tile")]
    [SerializeField]
    GameObject defaultMapTile = null;
    [SerializeField]
    public float mapAxisOffset = 0.7f;

    // List to store all the tiles
    List<GameObject> listOfTiles = new List<GameObject>();
    int lastTileIndex = 0;
    // Cache the last spawned position and depth of the tile prefab
    Vector3 lastSpawnPosition = Vector3.zero;
    float tileDepth = 0.0f;
    GameObject firstMostTile;
    // Camera Object
    GameObject cameraObject = null;
    Vector3 startingCameraPosition = Vector3.zero;


    
    void Awake()
    {
        // If Instance is already created
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        // Attach Instance
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        // Get the Camera and it's position
        cameraObject = Camera.main.gameObject;
        startingCameraPosition = cameraObject.transform.position;
        // Set the tile depth and spawning position
        tileDepth = defaultMapTile.transform.GetChild(0).GetComponent<BoxCollider>().size.y * 2.5f;
        lastSpawnPosition = new Vector3(-mapAxisOffset, 0, startingCameraPosition.z);

        // Generate the minimum required tiles
        for (int i = 0; i < minimumVisbileTiles; ++i)
        {
            // Get referrence to spawned tile
            GameObject temp = Instantiate(defaultMapTile, lastSpawnPosition, Quaternion.identity);
            temp.name = i.ToString();
            listOfTiles.Add(temp);

            lastSpawnPosition.x += mapAxisOffset; 
            // Do we need to generate the Obstacles?
            if (i > minimumStartObstacle)
            {
                ObstacleManager.Instance.SpawnRandomObstacle(temp, lastSpawnPosition);
                FoodManager.instance.SpawnRandomFood(temp, lastSpawnPosition);
            }
            lastSpawnPosition.x -= mapAxisOffset;

            // Reset Path bools
            PathManager.Instance.ResetPathTaken();
            // Set next positions
            lastSpawnPosition.Set(lastSpawnPosition.x, lastSpawnPosition.y, lastSpawnPosition.z + tileDepth);
        }
        // Set the first most Tile
        firstMostTile = listOfTiles[listOfTiles.Count - 1];
    }


    void Update()
    {
        // Check if we need to move the tile forward
        if((cameraObject.transform.position.z - listOfTiles[lastTileIndex].transform.position.z) > tileDepth)
        {
            MoveLastTileForward();
        }

        // Check if we need to reset
        if(PlayerManager.instance.transform.position.z >= MaxResetRange)
        {
            ResetPosition();
        }
    }


    // Moves the last unseen tile to the front for reuse
    public void MoveLastTileForward()
    {
        // Move the last Tile to the front
        listOfTiles[lastTileIndex].transform.position = lastSpawnPosition;
        // Cache the newest first tile
        firstMostTile = listOfTiles[lastTileIndex]; 
        // set the newest lastSpawnPosition for the next tile
        lastSpawnPosition.Set(lastSpawnPosition.x, lastSpawnPosition.y, lastSpawnPosition.z + tileDepth);
        

        // Update the afterSpawn Objects
        ObstacleManager.Instance.UpdateAfterSpawn();

        // Spawn Obstacle if can
        lastSpawnPosition.x += mapAxisOffset;  
        ObstacleManager.Instance.SpawnRandomObstacle(firstMostTile, lastSpawnPosition);
        // Spawn food
        FoodManager.instance.SpawnRandomFood(firstMostTile, lastSpawnPosition);
        lastSpawnPosition.x -= mapAxisOffset;

       
        
        // Get ready for next tile
        lastTileIndex++;
        if (lastTileIndex >= listOfTiles.Count)
            lastTileIndex = 0;
        // Reset the Lanes Taken bools
        PathManager.Instance.ResetPathTaken();
    }
    // Resets all the Tiles and Player back to (0,0,0) 
    public void ResetPosition()
    {
        // Reset player's position
        PlayerManager.instance.ResetPlayer();
        // Reset last spawned position
        lastSpawnPosition = new Vector3(-mapAxisOffset, 0, startingCameraPosition.z);

        // reset all tiles inFront of this Index
        for(int i = lastTileIndex; i < listOfTiles.Count; ++i)
        {
            listOfTiles[i].transform.position = lastSpawnPosition;
            // set the newest last spawn position
            lastSpawnPosition.Set(lastSpawnPosition.x, lastSpawnPosition.y, lastSpawnPosition.z + tileDepth);
        }
        // reset rest of tiles
        for (int i = 0; i < lastTileIndex; ++i)
        {
            listOfTiles[i].transform.position = lastSpawnPosition;
            // set the newest last spawn position
            lastSpawnPosition.Set(lastSpawnPosition.x, lastSpawnPosition.y, lastSpawnPosition.z + tileDepth);
        }
    }


    // Returns the position of the first most tile
    public Vector3 GetFirstMostTilePosition()
    {
        return firstMostTile.transform.position;
    }
    // Returns the position of the first most tile
    public Transform GetFirstMostTile()
    {
        return firstMostTile.transform;
    }
}
