﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance = null;

    [System.Serializable]
    public class AudioFile
    {
        public string audioName;
        public AudioClip audioClip;
    }

    [SerializeField]
    List<AudioFile> ListOfAudioFiles = new List<AudioFile>();

    [SerializeField]
    AudioSource BGMusic;
    [SerializeField]
    AudioSource SoundEffects;

    Dictionary<string, AudioClip> audioList = new Dictionary<string, AudioClip>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < ListOfAudioFiles.Count; ++i)
        {
            audioList.Add(ListOfAudioFiles[i].audioName, ListOfAudioFiles[i].audioClip);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool PlayBackgroundAudio(string audioName)
    {
        // Check if music is playing or if list contains a music file
        if (BGMusic.isPlaying || !audioList.ContainsKey(audioName))
            return false;

        // Set sthe clip to the target audio file
        var playingClip = audioList[audioName];

        // If the audio clip is not loaded
        if (playingClip == null)
            return false;

        BGMusic.clip = playingClip;
        BGMusic.Play();

        return true;
    }

    public bool PlaySoundEffect(string audioName)
    {
        // Check if music is playing or if list contains a music file
        if (SoundEffects.isPlaying || !audioList.ContainsKey(audioName))
            return false;

        // Set sthe clip to the target audio file
        var playingClip = audioList[audioName];

        // If the audio clip is not loaded
        if (playingClip == null)
            return false;

        SoundEffects.clip = playingClip;
        SoundEffects.Play();

        return true;

    }
}
