﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance = null;

    [Header("Player Speed")]
    [SerializeField]
     float playerSpeed = 5;

    [Header("Player Shifting Speed")]
    [SerializeField]
     float shiftSpeed = 25;

    private float internalShiftSpeed = 0;

    [Header("Player Jump Speed")]
    [SerializeField]
     float jumpSpeed = 5;

    private float internalJumpSpeed = 0;

    [Header("Distance between Lanes")]
    public float laneGap = 5;

    [SerializeField]
    private Vector3 StartingPosition;

    [SerializeField]
    CapsuleCollider MainCollider;
    [SerializeField]
    CapsuleCollider SlideCollider;

    

     Rigidbody playerRB;
     Vector3 TargetPosition;
     Vector3 tempPosition;
     bool isMoving = false;
     bool isFalling = false; 
     bool isJumping = false;
     bool isSliding = false;
     PathManager.PATHS currPath;
     PathManager.PathReturnData targetPath;
     PlayerStats statsRef;
    Animator playerAnim;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        playerRB = GetComponent<Rigidbody>();

        statsRef = GetComponent<PlayerStats>();

        currPath = PathManager.PATHS.P_CENTER;

        targetPath = PathManager.Instance.GetTargetPath(currPath);

        playerAnim = GetComponent<Animator>();

        // Set it to the center
        transform.position = new Vector3(targetPath.position.x, transform.position.y, transform.position.z);

        // Store the starting position of the player
        StartingPosition = transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        #region KeyboardControls
        // Player move left
        if (Input.GetKeyUp(KeyCode.A))
        {
            MoveLeft();
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            MoveRight();
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Jump();
        }

        if (Input.GetKeyUp(KeyCode.S))
        {
            Slide();
        }
        #endregion

        #region MobileControls
        if (Swipe.GetInstance().GetSwipeLeft())
        {
            MoveLeft();
        }
        else if (Swipe.GetInstance().GetSwipeRight())
        {
            MoveRight();
        }
        else if (Swipe.GetInstance().GetSwipeUp())
        {
            Jump();
        }
        else if (Swipe.GetInstance().GetSwipeDown())
        {
            Slide();
        }

        #endregion
        //Debug.Log(playerRB.velocity);
        //Debug.Log("Player Curr Path :" + (int)currPath);
    }

    // For physics movement
    private void FixedUpdate()
    {
        //// if (!ChangingLane)
        //     playerRB.velocity = transform.forward * Time.deltaTime * playerSpeed;
        // Move the player forward in a constant speed with playerSpeed
        // If the player wants to move left or right then internalShiftSpeed handles left and right movement

        // if the player is shifting to the left
        if (internalShiftSpeed < 0)
        {
            if (transform.position.x <= targetPath.position.x)
            {
                transform.position = new Vector3(targetPath.position.x, transform.position.y, transform.position.z);
                internalShiftSpeed = 0;
                isMoving = false;
            }
        }
        // else if the player is shifting to the right
        else if (internalShiftSpeed > 0)
        {
            if (playerRB.position.x >= targetPath.position.x)
            {
                transform.position = new Vector3(targetPath.position.x, transform.position.y, transform.position.z);
                internalShiftSpeed = 0;
                isMoving = false;
            }
        }
        

        if(internalJumpSpeed > 0)
        {
            if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
            {
                if (playerAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.5)
                {
                    isFalling = true;
                    if (internalJumpSpeed > 0)
                        internalJumpSpeed = -internalJumpSpeed * 1.5f;
                }
            }
             if (playerRB.position.y >= StartingPosition.y + 2)
            {
                isFalling = true;
                if (internalJumpSpeed > 0)
                    internalJumpSpeed = -internalJumpSpeed * 1.5f;

            }
        }
      

        if (internalJumpSpeed < 0)
        {
            if (playerRB.position.y <= StartingPosition.y)
            {
                internalJumpSpeed = 0;
                isFalling = false;
                isJumping = false;

                Vector3 newPos = transform.position;
                newPos.y = StartingPosition.y;
                transform.position = newPos;

            }
        }

        if (isSliding)
        {
            if(playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Slide"))
            {
                if(playerAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.8)
                {
                    isSliding = false;
                    MainCollider.enabled = true;
                    SlideCollider.enabled = false;
                }
            }
        }


        TargetPosition = transform.position + (transform.forward * playerSpeed * Time.deltaTime) + (transform.right * internalShiftSpeed * Time.deltaTime) + (transform.up * internalJumpSpeed * Time.deltaTime);
        playerRB.MovePosition(TargetPosition);

    }

    public void MoveLeft()
    {
        // If the player is moving
        if (isMoving)
            return;
        // If the player is already on the most left lane
        if ((int)currPath <= 0)
            return;

        currPath = (PathManager.PATHS)(int)(currPath - 1);

        targetPath = PathManager.Instance.GetTargetPath(currPath);

        internalShiftSpeed = -shiftSpeed;

       // tempPosition = transform.position;
        isMoving = true;
    }

    public void Slide()
    {
        if (isSliding)
            return;

        playerAnim.SetTrigger("m_tSlide");
        isSliding = true;
        SlideCollider.enabled = true;
        MainCollider.enabled = false;
    }

    public void MoveRight()
    {
        if (isMoving)
            return;
        // If the player is already on the most right lane
        if ((int)currPath >= 2)
            return;

        currPath = (PathManager.PATHS)(int)(currPath + 1);

        targetPath = PathManager.Instance.GetTargetPath(currPath);

        //TargetPosition = transform.position + new Vector3(laneGap, 0, 0);
        internalShiftSpeed = shiftSpeed;

       // tempPosition = transform.position;
        isMoving = true;
    }

    public void Jump()
    {
        if (isJumping || isFalling)
            return;

        internalJumpSpeed = jumpSpeed;
        playerAnim.SetTrigger("m_tJump");
        isJumping = true;
    }

    public void ResetPlayer()
    {
        transform.position = StartingPosition;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.tag == "Food")
        //{
        //    FoodBaseItem FoodItem = collision.gameObject.transform.parent.GetComponent<FoodBaseItem>();
        //    statsRef.SetHunger(statsRef.GetHunger() + FoodItem.hungerAmount);
        //    statsRef.AddEmission(FoodItem.carbonAmount);

        //    collision.gameObject.transform.parent.transform.parent = null;
        //    collision.gameObject.transform.parent.gameObject.SetActive(false);
        //    //Destroy(collision.gameObject.transform.parent.gameObject);
        //}
    }
}
