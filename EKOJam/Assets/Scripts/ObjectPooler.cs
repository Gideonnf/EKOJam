﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance = null;

    [System.Serializable]
    public class ObjectsToPool
    {
        public string objectTag;
        public GameObject prefab;
        public int NumberOfObjects;
    }

    public List<ObjectsToPool> ListOfObjectsToPool;
    private Dictionary<string, List<GameObject>> objectDictionary;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        objectDictionary = new Dictionary<string, List<GameObject>>();

        // Iterate through the list of objects that we need to create pool for
        foreach(ObjectsToPool objectsToPool in ListOfObjectsToPool)
        {
            // Create the list of the objects
            List<GameObject> GOList = new List<GameObject>();

            // Create the objects based on the number of objects 
            for(int i = 0; i < objectsToPool.NumberOfObjects; ++i)
            {
                GameObject newObject = Instantiate(objectsToPool.prefab);
                newObject.SetActive(false);
                // Add to the list
                GOList.Add(newObject);
            }
            // Add the list to the dictionary
            objectDictionary.Add(objectsToPool.objectTag, GOList);
        }
    }

    public GameObject FetchGO(string objectTag)
    {
        // Check if the dictionary contain the object tag
        if (!objectDictionary.ContainsKey(objectTag))
        {
            Debug.LogError("Doesn't exist in the list");
            return null;
        }
    
        // Iterate through the list of gameobjects for that tag
        foreach(GameObject GO in objectDictionary[objectTag])
        {
            // Retrieves the first object that is false
            if (GO.activeSelf == false)
            {
                GO.SetActive(true);
                return GO;
            }
        }

        // If it reaches here, it means that there is no more inactive objects in the list
        // Store the object that we need to create
        GameObject prefabToCreate = null;

        // Find the prefab that we need to create
        foreach(ObjectsToPool objectToPool in ListOfObjectsToPool)
        {
            // If it has the same object Tag
            if (objectToPool.objectTag == objectTag)
            {
                prefabToCreate = objectToPool.prefab;
                break;
            }
        }

        //If there is no prefab, shouldnt ever happen lol
        if(prefabToCreate == null)
        {
            return null;
        }

        // Create 5 more objects
        for(int i = 0; i < 5; ++i)
        {
            GameObject newObject = Instantiate(prefabToCreate); // Instantiate
            newObject.SetActive(false); // Set active to false
            objectDictionary[objectTag].Add(newObject); // Add to the list
        }

        // Return the last object in the list
        objectDictionary[objectTag][objectDictionary[objectTag].Count - 1].SetActive(true);
        return objectDictionary[objectTag][objectDictionary[objectTag].Count - 1];
    }

    public GameObject FetchGO(string objectTag, Vector3 newPosition)
    {
        // Check if the dictionary contain the object tag
        if (objectDictionary.ContainsKey(objectTag))
        {
            Debug.LogError("Doesn't exist in the list");
            return null;
        }

        GameObject objectToSpawn = null;

        // Iterate through the list of gameobjects for that tag
        foreach (GameObject GO in objectDictionary[objectTag])
        {
            // Retrieves the first object that is false
            if (GO.activeSelf == false)
            {
                objectToSpawn = GO;
                //GO.SetActive(true);
                //return GO;
            }
        }

        // Check if there is already an object ready to spawn
        if(objectToSpawn == null)
        {
            // If it reaches here, it means that there is no more inactive objects in the list
            // Store the object that we need to create
            GameObject prefabToCreate = null;

            // Find the prefab that we need to create
            foreach (ObjectsToPool objectToPool in ListOfObjectsToPool)
            {
                // If it has the same object Tag
                if (objectToPool.objectTag == objectTag)
                {
                    prefabToCreate = objectToPool.prefab;
                    break;
                }
            }

            //If there is no prefab, shouldnt ever happen lol
            if (prefabToCreate == null)
            {
                return null;
            }

            // Create 5 more objects
            for (int i = 0; i < 5; ++i)
            {
                GameObject newObject = Instantiate(prefabToCreate); // Instantiate
                newObject.SetActive(false); // Set active to false
                objectDictionary[objectTag].Add(newObject); // Add to the list
            }

            // Set the last object in the list as the object to spawn
           objectToSpawn = objectDictionary[objectTag][objectDictionary[objectTag].Count - 1];
        }

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = newPosition;

        return objectToSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
