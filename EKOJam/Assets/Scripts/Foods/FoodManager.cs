﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodManager : MonoBehaviour
{
    // Food Manager instance
    public static FoodManager instance = null;

    [SerializeField]
    private List<FoodBaseItem> ListOfFoods = new List<FoodBaseItem>();
    [SerializeField]
    float groundLevel = 0.0f;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    float spawnChance = 0.3f;

    private void Awake()
    {
        // Create Instance
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        // Update the spawn chance based on the player's hunger
    }

    public void SpawnRandomFood(GameObject parentObject, Vector3 centerLanePos)
    {
        float chanceToSpawn = 0.0f;
        chanceToSpawn = Random.value;

        // Check if food should spawn
        if (chanceToSpawn > spawnChance)
            return;

        Vector3 centerPosition = centerLanePos;
        // Set it to the floor
        centerPosition.y = groundLevel;

        bool stopLoop = false;
        //int loopCount = 0;

        while (!stopLoop)
        {
            chanceToSpawn = Random.value;

            // Check through all the food items
            foreach(FoodBaseItem foodItem in ListOfFoods)
            {
                // Check if it can spawn
                if(chanceToSpawn < foodItem.spawnChance)
                {
                    // Get a path
                    PathManager.PathReturnData returnData = PathManager.Instance.GetAvaliableRandomPath(centerPosition);
                    // If there is no open path
                    if (returnData.pathDirEnum == PathManager.PATHS.P_TOTAL)
                    {
                        stopLoop = true;
                        break;
                    }
                     
                    // The food can be spawned
                    GameObject spawnedFood;
                    spawnedFood = ObjectPooler.instance.FetchGO(foodItem.FoodTag);
                    spawnedFood.transform.position = returnData.position;
                    Debug.Log("Path : " + returnData.pathDirEnum + " Pos : " + (returnData.position));
                    spawnedFood.transform.parent = parentObject.transform;

                    stopLoop = true;
                    break;
                }
            }

            //loopCount++;
            //if (loopCount >= 20)
            //{

            //}

        }
    }
}
