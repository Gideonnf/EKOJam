﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBaseItem : MonoBehaviour
{
    [Range(0.0f, 1.0f)]
    public float spawnChance = 0.5f;

    // Hunger amount
    public float hungerAmount = 1;
    // Carbon amount
    public float carbonAmount = 1;

    [SerializeField]
    float rotateSpeed;

    public string FoodTag = "";

    [System.NonSerialized]
    public Vector3 spawnedPos;
    [System.NonSerialized]
    public PathManager.PATHS PathDir;

    private Vector3 m_EulerAngleVelocity;

    private GameObject cameraRef = null;

    PlayerStats statsRef;
    PlayerManager playerRef;


    public void Start()
    {
        m_EulerAngleVelocity = new Vector3(0, rotateSpeed, 0);
        cameraRef = Camera.main.gameObject;
        playerRef = PlayerManager.instance;
        statsRef = playerRef.GetComponent<PlayerStats>();
    }

    public void Update()
    {
        Quaternion targetRotation = Quaternion.Euler(m_EulerAngleVelocity * Time.deltaTime);
        transform.Rotate(targetRotation.eulerAngles);

        // If the objects are behind the camera
        if ((cameraRef.transform.position.z - transform.position.z) > 1.0f)
        {
            transform.parent = null;
            transform.gameObject.SetActive(false);
        }

    }

    // Collision
    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        statsRef.SetHunger(statsRef.GetHunger() + hungerAmount);
    //        statsRef.AddEmission(carbonAmount);

    //        //Unparent the object from the tile
    //        //Parent of the parent
    //        this.transform.parent = null;
    //        this.gameObject.SetActive(false);

    //        //Destroy(this.gameObject);
    //        //PlayerManager.instance.GetComponent<PlayerStats>().SetHunger(PlayerManager.instance.GetComponent<PlayerStats>().GetHunger() + hungerAmount);
    //    }
    //}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            statsRef.SetHunger(statsRef.GetHunger() + hungerAmount);
            statsRef.AddEmission(carbonAmount);

            //Unparent the object from the tile
            //Parent of the parent
            this.transform.parent = null;
            this.gameObject.SetActive(false);

            //Destroy(this.gameObject);
            //PlayerManager.instance.GetComponent<PlayerStats>().SetHunger(PlayerManager.instance.GetComponent<PlayerStats>().GetHunger() + hungerAmount);
        }
    }
}

