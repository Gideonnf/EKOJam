﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour
{
    
    [SerializeField]
    private float Hunger, CarbonEmission;
    float originalHunger, origianlEmission;
    [Header("Max Stats")]
    [SerializeField]
    float maxCarbonEmission;
    [SerializeField]
    float maxHunger;

    [Header("Hunger Decay")]
    public float decaySpeed = 2;

    [Header("Emission Deduction")]
    public float EmissionDeduction = 50;

    // Start is called before the first frame update
    void Start()
    {
        originalHunger = Hunger;
        origianlEmission = CarbonEmission;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Hunger);
        // Continously decay the hunger bar
        Hunger -= decaySpeed * Time.deltaTime;

    }

    public void SetHunger(float newHunger)
    {
        Hunger = newHunger;
    }
    public void ModifyHunger(float amount)
    {
        Hunger += amount;
    }

    // Getters
    public float GetHunger()
    {
        return Hunger;
    }
    public float GetCarbonEmission()
    {
        return CarbonEmission;
    }
    public float GetMaxHunger()
    {
        return maxHunger;
    }
    public float GetMaxCarbonEmission()
    {
        return maxCarbonEmission;
    }


    public void AddEmission(float EmissionToAdd)
    {
        CarbonEmission += EmissionToAdd;
    }

    // Every fixed distance, the emission will be reduced by a fixed amount.
    // Call this function to reduce it by a fixed amount
    public void ReduceEmission()
    {
        CarbonEmission -= EmissionDeduction;
    }
}
