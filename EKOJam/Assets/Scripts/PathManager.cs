﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour
{
    public static PathManager Instance = null;

   
    // Used to referrence the Paths
    public enum PATHS
    {
        P_LEFT,
        P_CENTER,
        P_RIGHT,

        P_TOTAL
    }
    // Used to return both the Dir enum and the position
    public struct PathReturnData
    {
        public PATHS pathDirEnum;
        public Vector3 position; 
    }
    // Attach the transform for the paths
    [Header("Paths")]
    [SerializeField]
    Transform left = null;
    bool leftTaken = true;
    [SerializeField]
    Transform center = null;
    bool centerTaken = false;
    [SerializeField]
    Transform right = null;
    bool rightTaken = true;
    [Header("Width Between Paths")]
    [SerializeField]
    float widthBetweenPaths = 5.0f;

    // Attach the Instance
    private void Awake()
    {
        // If Instance is already created
        if(Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        // Attach Instance
        Instance = this;

        // Set the path's width
        left.position.Set(center.position.x - widthBetweenPaths, left.position.y, left.position.z);
        right.position.Set(center.position.x + widthBetweenPaths, right.position.y, right.position.z);
    }


    // Get a random Avaliable Path
    // Returns Vector3.zero and PATH.P_TOTAL if no path to use
    public PathReturnData GetAvaliableRandomPath(Vector3 centerPosition)
    {
        // Create the Struct to return
        PathReturnData returnData;
        returnData.pathDirEnum = PATHS.P_TOTAL;
        returnData.position = Vector3.zero;

        Vector3 newPos = centerPosition;
        // check for any avaliable paths
        if (centerTaken && leftTaken && rightTaken)
            return returnData;

        switch ((PATHS)Random.Range(0,3))
        {
            case PATHS.P_LEFT:
                {
                    // if haven't taken yet
                    if(!leftTaken)
                    {
                        newPos.x -= widthBetweenPaths;
                        leftTaken = true;
                        returnData.pathDirEnum = PATHS.P_LEFT;
                        break;
                    }
                    // left taken, so now check right
                    else if (!rightTaken)
                    {
                        newPos.x += widthBetweenPaths;
                        rightTaken = true;
                        returnData.pathDirEnum = PATHS.P_RIGHT;
                        break;
                    }
                    // left taken, so now check center
                    else if (!centerTaken)
                    {
                        centerTaken = true;
                        returnData.pathDirEnum = PATHS.P_CENTER;
                        break;
                    }
                }
                
                break;
            case PATHS.P_RIGHT:
                {
                    // if haven't taken yet
                    if (!rightTaken)
                    {
                        newPos.x += widthBetweenPaths;
                        rightTaken = true;
                        returnData.pathDirEnum = PATHS.P_RIGHT;
                        break;
                    }
                    // right taken, so now check left
                    else if (!leftTaken)
                    {
                        newPos.x -= widthBetweenPaths;
                        leftTaken = true;
                        returnData.pathDirEnum = PATHS.P_LEFT;
                        break;
                    }
                    // right taken, so now check center
                    else if (!centerTaken)
                    {
                        centerTaken = true;
                        returnData.pathDirEnum = PATHS.P_CENTER;
                        break;
                    }
                }
                break;

            case PATHS.P_CENTER:
                {
                    // if center hsa been taken, check other direction
                    if (centerTaken)
                    {
                        // center taken, so now check right
                        if (!rightTaken)
                        {
                            newPos.x += widthBetweenPaths;
                            rightTaken = true;
                            returnData.pathDirEnum = PATHS.P_RIGHT;
                            break;
                        }
                        // center taken, so now check left
                        else if (!leftTaken)
                        {
                            newPos.x -= widthBetweenPaths;
                            leftTaken = true;
                            returnData.pathDirEnum = PATHS.P_LEFT;
                            break;
                        }
                    }
                    else
                    {
                        // center now taken
                        centerTaken = true;
                        returnData.pathDirEnum = PATHS.P_CENTER;
                    }
                }
                break;
        }
        // set the pos
        returnData.position = newPos;

        return returnData;
    }

    public PathReturnData GetTargetPath(PATHS targetPath)
    {
        // Create the Struct to return
        PathReturnData returnData;
        returnData.pathDirEnum = PATHS.P_TOTAL;
        returnData.position = Vector3.zero;

        Vector3 newPos = center.position;
        
        switch(targetPath)
        {
            case PATHS.P_LEFT:
                {
                    newPos = left.position;
                    returnData.pathDirEnum = PATHS.P_LEFT;
                    break;
                }
            case PATHS.P_CENTER:
                {
                    newPos = center.position;
                    returnData.pathDirEnum = PATHS.P_CENTER;

                    break;
                }
            case PATHS.P_RIGHT:
                {
                    newPos = right.position;
                    returnData.pathDirEnum = PATHS.P_RIGHT;

                    break;
                }
        }

        returnData.position = newPos;

        return returnData;
    }

    // Gets a Path if avaliable and reserves it for you
    public bool GetAPath(PATHS whatPath)
    {
        switch (whatPath)
        {
            case PATHS.P_LEFT:
                {
                    if (leftTaken)
                        return false;

                    leftTaken = true;
                    return true;
                }
            case PATHS.P_CENTER:
                {
                    if (centerTaken)
                        return false;

                    centerTaken = true;
                    return true;
                }
            case PATHS.P_RIGHT:
                {
                    if (rightTaken)
                        return false;

                    rightTaken = true;
                    return true;
                }
        }

        return false;
    }
    // Reset bools that guard the paths
    public void ResetPathTaken()
    {
        centerTaken = false;
        rightTaken = false;
        leftTaken = false;
    }
    // Get the Width Between paths
    public float GetWidthBetweenPaths()
    {
        return widthBetweenPaths;
    }
}
